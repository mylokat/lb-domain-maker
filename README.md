# Domain maker for laravel-boilerplate 8 by rappasoft

Creates a model, migration, controller in provided domain

## Requires

- [Laravel 8](https://laravel.com)
- [rappasoft/laravel-boilerplate (version 8)](https://www.github.com/rappasoft/laravel-boilerplate/)

## Install

```
composer require lokat/lb-domain-maker
```

## Run

In your Laravel project root folder:

```
php artisan domain:make Example
```

The command will create a model with _Example_ name in app/Domains/Example/Models folder. 

## Options

You can create the domain and run the migration by running the command with the --migrate option:
```
php artisan domain:make Example --migration

or

php artisan domain:make Example -m
```

You may also specify the name of the model by running the command with the --model option:
```
php artisan domain:make Example --model=ModelName

or

php artisan domain:make Example -M ModelName
```

If you want to create a controller, run the command with --controller option:
```
php artisan domain:make Example --controller

or

php artisan domain:make Example -c
```
The controller will be created on _app/Domains/Example/Http/Controllers/Backend/ExampleController_

You may also create the controller for frontend by running the command with --frontend option:
```
php artisan domain:make Example --controller --frontend

or

php artisan domain:make Example -c -f
```

You may also create the controller with resource by running the command with --resource option:
```
php artisan domain:make Example --resource

or

php artisan domain:make Example -r
```

You may want to create the controller without the model by running the command with --no-model option:
```
php artisan domain:make Example --controller --no-model 

or 

php artisan domain:make Example -cN
```

Add service 
```
php artisan domain:make Example --service 

or 

php artisan domain:make Example -s
```
It will create service at 
```
app/Domains/Example/Services/ExampleService.php 
```

Add observer 
```
php artisan domain:make Example --observer 

or 

php artisan domain:make Example -o
```
It will create observer at 
```
app/Domains/Example/Observers/ExampleObserver.php 
```

Add model resource 
```
php artisan domain:make Example --model-resource 

or 

php artisan domain:make Example -R
```
It will create model resource at 
```
app/Domains/Example/Resources/ExampleResource.php 
```

Add request 
```
php artisan domain:make Example --request 

or 

php artisan domain:make Example -t
```
It will create 2 form requests at 
```
app/Domains/Example/Requests/ExampleStoreRequest.php 
app/Domains/Example/Requests/ExampleUpdateRequest.php 
```

Add livewire datatable 
```
php artisan domain:make Example --datatable 

or 

php artisan domain:make Example -d
```
It will create 2 livewire datatables at 
```
app/Domains/Example/Livewire/Frontend/ExamplesTable.php 
app/Domains/Example/Livewire/Backend/ExamplesTable.php 
```

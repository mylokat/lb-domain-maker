<?php

namespace Lokat\LBDomainMaker\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Database\Eloquent\Model;

class DomainMakerCommand extends GeneratorCommand
{
    private $domain, $model, $baseNamespace;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'domain:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Domain Model/Controller/Migration/Services/Observer';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->domain = $this->argument('name');
        $this->model = $this->domain;
        $this->baseNamespace = 'App/Domains/'.$this->domain;

        if ($this->option('model')) {
            $this->model = $this->option('model');
        }

        if (!$this->option('no-model')) {
            $this->createModel();
        }

        if ($this->option('migration')) {
            $this->createMigration();
        }

        if ($this->option('controller') || $this->option('resource')) {
            $this->createController();
        }

        if ($this->option('service')) {
            $this->createService();
        }

        if ($this->option('observer')) {
            $this->createObserver();
        }

        if ($this->option('request')) {
            $this->createRequest();
        }

        if ($this->option('datatable')) {
            $this->createDataTable();
        }

        if ($this->option('model-resource')) {
            $this->createResource();
        }

        return 0;
    }

    protected function createModel()
    {
        $modelNamespace = $this->baseNamespace.'/Models/';

        $this->call('make:model', [
            'name' => $modelNamespace.$this->model
        ]);
    }

    protected function createMigration()
    {
        $table = Str::snake(Str::pluralStudly(class_basename($this->model)));

        if ($this->option('pivot')) {
            $table = Str::singular($table);
        }

        $this->call('make:migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
        ]);
    }

    protected function createController()
    {
        $controllerNamespace = $this->baseNamespace.'/Http/Controllers/';
        $controller = $controllerNamespace.'Backend/'.Str::studly(class_basename($this->model));

        $modelName = 'App/Domains/'.$this->argument('name').'/Models/'.$this->model;

        $this->call('make:controller', array_filter([
            'name' => "{$controller}Controller",
            '--model' => $this->option('resource') && $this->alreadyExists($modelName) ? $modelName : null,
            '--resource' => $this->option('resource') && !$this->alreadyExists($modelName) ? true : null,
        ]));

        if ($this->option('frontend')) {
            $frontendController = $controllerNamespace.'Frontend/'.Str::studly(class_basename($this->model));

            $this->call('make:controller', array_filter([
                'name' => "{$frontendController}Controller",
                '--model' => $this->option('resource') && $this->alreadyExists($modelName) ? $modelName : null,
                '--resource' => $this->option('resource') && !$this->alreadyExists($modelName) ? true : null,
            ]));

        }

    }

    protected function createService()
    {
        $paths = [
            '' => base_path('app/Domains/'.$this->domain.'/Services/'.$this->model.'Service.php'),
//            'Backend' => base_path('app/Domains/'.$this->domain.'/Services/Backend/'.$this->model.'Service.php'),
//            'Frontend' => base_path('app/Domains/'.$this->domain.'/Services/Frontend/'.$this->model.'Service.php')
        ];

        foreach ($paths as $path) {
            $this->makeDirectory($path);
        }

        $contents = [
            '' => $this->getStubSourceFile(),
//            'Backend' => $this->getStubSourceFile('Backend', 'sub'),
//            'Frontend' => $this->getStubSourceFile('Frontend', 'sub'),
        ];

        foreach ($contents as $i => $content) {
            if (!$this->files->exists($paths[$i])) {
                $this->files->put($paths[$i], $content);
                $this->info(($i ? $i."/" : "")."{$this->model}Service created");
            } else {
                $this->info(($i ? $i."/" : "")."{$this->model}Service already exits");
            }
        }

    }

    protected function createObserver()
    {
        $paths = [
            '' => base_path('app/Domains/'.$this->domain.'/Observers/'.$this->model.'Observer.php'),
        ];

        foreach ($paths as $path) {
            $this->makeDirectory($path);
        }

        $contents = [
            '' => $this->getStubSourceFile('', 'observer'),
        ];

        foreach ($contents as $i => $content) {
            if (!$this->files->exists($paths[$i])) {
                $this->files->put($paths[$i], $content);
                $this->info(($i ? $i."/" : "")."{$this->model}Observer created");
            } else {
                $this->info(($i ? $i."/" : "")."{$this->model}Observer already exits");
            }
        }
    }

    protected function createResource()
    {
        $paths = [
            '' => base_path('app/Domains/'.$this->domain.'/Resources/'.$this->model.'Resource.php'),
        ];

        foreach ($paths as $path) {
            $this->makeDirectory($path);
        }

        $contents = [
            '' => $this->getStubSourceFile('', 'resource'),
        ];

        foreach ($contents as $i => $content) {
            if (!$this->files->exists($paths[$i])) {
                $this->files->put($paths[$i], $content);
                $this->info(($i ? $i."/" : "")."{$this->model}Resource created");
            } else {
                $this->info(($i ? $i."/" : "")."{$this->model}Resource already exits");
            }
        }
    }

    protected function createRequest()
    {
        $paths = [
            'Store' => base_path('app/Domains/'.$this->domain.'/Http/Requests/'.$this->model.'StoreRequest.php'),
            'Update' => base_path('app/Domains/'.$this->domain.'/Http/Requests/'.$this->model.'UpdateRequest.php'),
        ];

        foreach ($paths as $path) {
            $this->makeDirectory($path);
        }

        $contents = [
            'Store' => $this->getStubSourceFile('Store', 'request'),
            'Update' => $this->getStubSourceFile('Update', 'request'),
        ];

        foreach ($contents as $i => $content) {
            if (!$this->files->exists($paths[$i])) {
                $this->files->put($paths[$i], $content);
                $this->info(($i ? $i."/" : "")."{$this->model}{$i}Request created");
            } else {
                $this->info(($i ? $i."/" : "")."{$this->model}{$i}Request already exits");
            }
        }
    }

    protected function createDataTable()
    {
        $paths = [
            'Frontend' => base_path('app/Domains/'.$this->domain.'/Http/Livewire/Frontend/'.Str::plural($this->model).'Table.php'),
            'Backend' => base_path('app/Domains/'.$this->domain.'/Http/Livewire/Backend/'.Str::plural($this->model).'Table.php'),
        ];

        foreach ($paths as $path) {
            $this->makeDirectory($path);
        }

        $contents = [
            'Frontend' => $this->getStubSourceFile('Frontend', 'livewire'),
            'Backend' => $this->getStubSourceFile('Backend', 'livewire'),
        ];

        foreach ($contents as $i => $content) {
            if (!$this->files->exists($paths[$i])) {
                $this->files->put($paths[$i], $content);
                $this->info(($i ? $i."/" : "").Str::plural($this->model)." Livewire Table created");
            } else {
                $this->info(($i ? $i."/" : "").Str::plural($this->model)." Livewire Table already exits");
            }
        }
    }

    protected function getStubFile($filename = 'mainservice')
    {
        return $this->resolveStubPath('/stubs/'.$filename.'.stub');
    }

    /**
     **
     * Map the stub variables present in stub to its value
     *
     * @return array
     *
     */
    public function getStubVariables($type = '')
    {
        return [
            'domain' => $this->domain,
            'model' => $this->model,
            'type' => $type,
            'models' => Str::plural($this->model),
            'columns' => $this->generateColumns('App\Domains\\'.$this->domain.'\Models\\'.$this->model)
        ];
    }

    /**
     * Get the stub path and the stub variables
     *
     * @return bool|mixed|string
     *
     */
    public function getStubSourceFile($type = '', $filename = 'mainservice')
    {
        return $this->getStubContents($this->getStubFile($filename), $this->getStubVariables($type));
    }


    /**
     * Replace the stub variables(key) with the desire value
     *
     * @param $stub
     * @param array $stubVariables
     * @return bool|mixed|string
     */
    public function getStubContents($stub , $stubVariables = [])
    {
        $contents = file_get_contents($stub);

        foreach ($stubVariables as $search => $replace)
        {
            $contents = str_replace('{{ '.$search.' }}' , $replace, $contents);
        }

        return $contents;

    }

    protected function getStub()
    {
        return $this->option('pivot')
            ? $this->resolveStubPath('/stubs/model.pivot.stub')
            : $this->resolveStubPath('/stubs/model.stub');
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param  string  $stub
     * @return string
     */
    protected function resolveStubPath($stub)
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__.$stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return is_dir(app_path('Models')) ? $rootNamespace.'\\Models' : $rootNamespace;
    }

    /**
     * Determine if the class already exists.
     *
     * @param  string  $rawName
     * @return bool
     */
    protected function alreadyExists($rawName)
    {
        return $this->files->exists($this->getPath($this->qualifyClass($rawName)));
    }

    /**
     * @throws \Exception
     */
    private function generateColumns(string $modelName): string
    {
        $model = new $modelName();

        if ($model instanceof Model === false) {
            throw new \Exception('Invalid model given.');
        }

        $getFillable = array_merge(
            [$model->getKeyName()],
            $model->getFillable(),
            ['created_at', 'updated_at']
        );

        $columns = "[\n";

        foreach ($getFillable as $field) {
            if (in_array($field, $model->getHidden())) {
                continue;
            }

            $title = Str::of($field)->replace('_', ' ')->ucfirst();

            $columns .= '            Column::make("'.$title.'", "'.$field.'")'."\n".'                ->sortable(),'."\n";
        }

        $columns .= '        ];';

        return $columns;
    }

    protected function getOptions()
    {
        return [
            ['controller', 'c', InputOption::VALUE_NONE, 'Create a new controller for the model'],
            ['migration', 'm', InputOption::VALUE_NONE, 'Create a new migration file for the model'],
            ['no-model', 'N', InputOption::VALUE_NONE, 'Create a domain without a model class'],
            ['pivot', 'p', InputOption::VALUE_NONE, 'Indicates if the generated model should be a custom intermediate table model'],
            ['resource', 'r', InputOption::VALUE_NONE, 'Indicates if the generated controller should be a resource controller'],
            ['frontend', 'f', InputOption::VALUE_NONE, 'Indicates if the generated controller should create a frontend controller'],
            ['model', 'M', InputOption::VALUE_OPTIONAL, 'Create a new model on domain'],
            ['service', 's', InputOption::VALUE_NONE, 'Create a new service on domain'],
            ['observer', 'o', InputOption::VALUE_NONE, 'Create a new observer on domain'],
            ['request', 't', InputOption::VALUE_NONE, 'Create a new request on domain'],
            ['datatable', 'd', InputOption::VALUE_NONE, 'Create a new livewire datatable on domain'],
            ['model-resource', 'R', InputOption::VALUE_NONE, 'Create a new model resource on domain'],
        ];
    }
}

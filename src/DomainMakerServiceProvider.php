<?php

namespace Lokat\LBDomainMaker;

use Illuminate\Support\ServiceProvider;

class DomainMakerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('Lokat\LBDomainMaker\Console\Commands\DomainMakerCommand');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
